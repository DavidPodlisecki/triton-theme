<?php
/**
 * The template for displaying 404 errors
 *
 */


get_header(); 

?>

		<div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						Not Found
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content single">
        
        	<p>The page you are looking for cannot be found.</p>
        
		</div>
	

<?php get_footer(); ?>