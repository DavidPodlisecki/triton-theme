<?php
/*
Template Name: Accessories
*/


get_header(); 

?>
		<?php get_template_part('templates/pagehead', 'accessory'); ?>
        
    
		<div class="section span_11 content single">
        
        	<div class="span_12 group">
        	
        	<?php $page = get_page_by_title( 'Careers' ); ?>
        <?php echo apply_filters( 'the_content', $page->post_content ); ?>
    
    <?php query_posts('post_type=careers&post_status=publish&posts_per_page=20&paged='. get_query_var('paged')); ?>
            
	<?php if( have_posts() ): ?>

        <?php while( have_posts() ): the_post(); ?>

	    <div class="span_12 col careerPost">
        
			
            
            <div class="span_12 section job">
                
                <h5><?php the_title(); ?></h5>
				<?php the_content(); ?>
                
            </div><!-- /#post-<?php get_the_ID(); ?> -->
            
            <div class="postnavigation">
			<div class="newer"><?php previous_posts_link(__('« newer posts','example')) ?></div> <div class="older"><?php next_posts_link(__('older posts »','example')) ?></div>
		</div><!-- /.navigation -->
            
       	</div>

        <?php endwhile; ?>

	<?php else: ?>

		<div id="post-404" class="noposts span_9 col">

		    <p>No jobs are available at this time.</p>

	    </div><!-- /#post-404 -->

	<?php endif; wp_reset_query(); ?>
    
    	
            
            
            </div>
        
		</div>
	

<?php get_footer(); ?>