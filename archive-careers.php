<?php
/*
Template Name: Carrers
*/


get_header(); 

?>
		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>		

		<div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						Careers
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content single">
        
        	<div class="span_12 group">
        	
        	<?php $page = get_page_by_title( 'Careers' ); ?>
        <?php echo apply_filters( 'the_content', $page->post_content ); ?>
    
    <?php query_posts('post_type=careers&post_status=publish&posts_per_page=20&paged='. get_query_var('paged')); ?>
            
	<?php if( have_posts() ): ?>

        <?php while( have_posts() ): the_post(); ?>

	    <div class="span_12 col careerPost">
        
			
            
            <div class="span_12 section job">
                
                <h5><?php the_title(); ?></h5>
				<?php the_content(); ?>
                
            </div><!-- /#post-<?php get_the_ID(); ?> -->
            
            <div class="postnavigation">
			<div class="newer"><?php previous_posts_link(__('« newer posts','example')) ?></div> <div class="older"><?php next_posts_link(__('older posts »','example')) ?></div>
		</div><!-- /.navigation -->
            
       	</div>

        <?php endwhile; ?>

	<?php else: ?>

		<div id="post-404" class="noposts span_9 col">

		    <p>No jobs are available at this time.</p>

	    </div><!-- /#post-404 -->

	<?php endif; wp_reset_query(); ?>
    
    	
            
            
            </div>
        
		</div>
	

<?php get_footer(); ?>