<?php
/*
Template Name: FAQs
*/



get_header(); 


?>

		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>			
					
		
		<div id="header" class="span_12 section trailers" style="margin-bottom:40px">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>

						FAQs
                    
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content" id="faqPage">
        <form class="span_12 group faqSearch">
        <?php
$terms = get_terms("FAQ_category");
 $count = count($terms);
 if ( $count > 0 ){
     echo "<select id='filter-select' name='faqsearch' class='span_6 col' >" ;
echo "<option value='all' class='selected'>Select Category</option>";
     foreach ( $terms as $term ) {
         echo "<option value='{$term->slug}'>" . $term->name . "</option>";
     }
     echo "</select>";
 }
?>
</form>

 
    <script>
    function faqselect(){
       var faq = $( "select[name=faqsearch] option:selected").val();
        //alert(faq);
        //console.log(faq);
        $.get( "/faq-results/", { f: faq} )
            .done(function( data ) {
            //alert( "Data Loaded: " + data );
            $('#theresults').empty();
            $('#theresults').append(data);
         });
                
    }
    
    // pagination functions
    
    var curPage = 0;
		function getRows(page){
			$('.prev, .next').hide();
			var rows = $('#theresults .faq');
			var maximum = 10;
			var lastRow = rows.length;
			var pages = Math.ceil(lastRow/maximum);
			var start = page * maximum;
			var end = maximum + start;
			//console.log(pages);
			if(rows.length >= maximum+1){
				if(start == 0){
					$('.next').show();
					$('#theresults .faq').hide(0,function(){
					for(i=0; i < maximum; i++){
						$('#theresults .faq:eq('+i+')').show();
						}						
						});
					
					}
				else if(start >= 1 && end < lastRow){
					$('.prev,.next').show();
					$('#theresults .faq').hide(0, function(){
					for(i=start; i < [start+maximum]; i++){
						$('#theresults .faq:eq('+i+')').show();
						}
						});
					}
				else{
					$('.next').hide();
					$('.prev').show();
					$('#theresults .faq').hide(0, function(){
					for(i=start; i < [start+maximum]; i++){
						$('#theresults .faq:eq('+i+')').show();
						}
						});
					}
				}
			else{
				$('#theresults .faq').show();
				$('.prev, .next').hide();
				}
				
			}
		function nextPage(){
			$('#theresults .faq').show();
			curPage++;
			getRows(curPage);
			var top = $('.faqSearch').offset().top;
			$('html,body').animate({scrollTop:top},300);
			}
		function prevPage(){
			$('#theresults .faq').show();
			curPage--;
			getRows(curPage);
			var top = $('.faqSearch').offset().top;
			$('html,body').animate({scrollTop:top},300);
			}
		
		$(document).ajaxComplete(function(){
				curPage=0;
				getRows(curPage);
			});
    
    
    
     $(window).load(function(){
                  $('.prev,.next').hide();
                  faqselect();
             });
     $('#filter-select').change(function(){
      
                  faqselect();
    });

           
                  </script>
                 <div id="theresults"></div>
            <a class="prev" onclick="prevPage()"><< Previous</a><a class="next right" onclick="nextPage()">Next >></a>
         </div>

<?php get_footer(); ?>