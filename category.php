<?php
/**
 * The template for displaying posts within a category
 *
 */


get_header(); 

?>

		<div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
                    <?php 
						// Check if there are any posts to display
						if ( have_posts() ) : ?>
						<?php single_cat_title(); ?>
                        <?php else: ?>
                        	No Results
                        <?php endif; ?>
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content single">
        
        <div class="span_12 group">
             <div class="span_9 col">
                  
                  <?php 
					// Check if there are any posts to display
					if ( have_posts() ) : 
                    
                    while ( have_posts() ) : the_post();?>
                    
                      <div class="span_12 section post">
                      
                      <?php if(the_post_thumbnail()){ ?>
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(200,220) ); ?></a>
                      <?php } 
                              else{$the_video_output = getFeaturedVideo($post->ID);
                                  ?>
                                  <?php 
                                       
                                      echo $the_video_output;
                                  ?>
                          <?php } ?>
                      
                      <h5><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
                     
                      
                      
                      <?php the_excerpt(); ?>
                      <a href="<?php the_permalink() ?>" class="moreLink">read more</a>
                       <span class="postDate-main"><?php the_time('F jS, Y') ?></span>
                      </div>
                      
                      <?php endwhile; 
                      
                      else: ?>
                      <p>Sorry, no posts matched your criteria.</p>
                      
                      
                      <?php endif; ?>
                    
              </div>
              
              <div class="span_3 col">
              	<?php get_sidebar(); ?>
              </div>
              
             </div>    
		</div>
	

<?php get_footer(); ?>