<?php if($comments) : ?>
	<ul class="span_12 section" id="postedComments">
  	<?php foreach($comments as $comment){ ?>
		<li id="comment-<?php comment_ID(); ?>" class="span_12 group">
		<div class="authorAvatar"><?php echo get_avatar($author_email, $size, $default_avatar ); ?></div>
        <div class="authorInfo"><strong><?php comment_author_link(); ?></strong><cite class="postdate"><?php comment_date(); ?> at <?php comment_time(); ?></cite></div>
			<?php if($comment->comment_approved == '0'){ ?>
				Your comment is awaiting approval
			<?php } else{ ?>
            			<div class="span_12 section commentText"><?php comment_text(); ?></div>
            <?php } ?>
            
		</li>
	<?php } ?>
	</ul>	
<?php endif; ?>