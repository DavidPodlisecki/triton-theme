<?php
/*
Template Name: faq results
*/

$faq_type = $_GET["f"];


if ($faq_type == 'all'){
$args = array(
	'post_type' => 'faqs',
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC'
);

}
else {

$args = array(
	'post_type' => 'faqs',
	'posts_per_page' => -1,
	'orderby'=> 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'FAQ_category',
			'field'    => 'slug',
			'terms'    => $faq_type
		),	
	),

);

} 
              
//$query = new WP_Query( $args );

// The Query
$wp_query = new WP_Query( $args );
// The Loop
if ( $wp_query->have_posts() ) { 
	while ( $wp_query->have_posts() ) {
		$wp_query->the_post(); ?>
		<div class="span_12 section faq">
             <p class="bold"><?php the_title(); ?> </p>
              <?php the_content(); ?>
     </div>
     
	<?php } ?>
	
<?php } else {
	echo 'No FAQ Found';
}
/* Restore original Post Data */
wp_reset_postdata();

               

                    
                 
