<?php wp_footer(); ?>
    
    
    </div>
    <div id="footer" class="section span_12">
    		<div class="section span_12 content">
            
            	<ul class="span_12 group footContent">
                
                	<li class="span_3 colWrap">
                    
                    	<a href="/" class="span_8 pos center homeLink">
                			<img src="/wp-content/themes/triton/img/logo.png" alt="Triton Trailers Logo" class="full-width center logo"> <!-- SITE LOGO -->
                		</a>
                        
                        <div id="social" class="span_8 pos center group">
                        
                        	<a href="https://www.facebook.com/pages/Triton-Trailers/123721674316653" class="span_6 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_fb.png" alt="Facebook" class="center">
                            </a>
                            <a href="https://www.youtube.com/user/TritonTrailers" class="span_6 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_yt.png" alt="YouTube" class="left">
                            </a>
                            
                            <!--<a href="https://www.facebook.com/pages/Triton-Trailers/123721674316653" class="span_3 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_fb.png" alt="Facebook" class="center">
                            </a>
                            <a href="https://www.youtube.com/user/TritonTrailers" class="span_3 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_yt.png" alt="YouTube" class="center">
                            </a>
                            <a class="span_3 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_tw.png" alt="Facebook" class="center">
                            </a>
                            <a class="span_3 col" target="_blank">
                            	<img src="/wp-content/themes/triton/img/foot_gp.png" alt="YouTube" class="center">
                            </a>-->
                        
                        </div>
                        
                    </li>
                    
                    <li class="span_3 colWrap">
                    	<form method="post" action="/find-a-dealer/" enctype="multipart/form-data" class="section span_12 group">
                            <label class="title">Find a dealer</label>
                            <input type="text" name="zip" placeholder="Zip/Postal">
                            <input type="submit" value="submit" class="submit">
                        </form>
                        <a href="http://www.tritontrailers.com/DistributorSuite/login.aspx?url=%2fdistributorsuite%2fDefault.aspx" class="title" target="_blank">distributor login</a>
                    </li>
                    
                    <li class="span_3 colWrap">
                    	<p class="title">Products</p>
                    	<?php wp_nav_menu(array('menu' => 'Main Menu')); ?>
                    </li>
                    
                    <li class="span_3 colWrap">
                    	<p class="title">About</p>
                    	<?php wp_nav_menu(array('menu' => 'Secondary Menu')); ?>
                    </li>
                    
                </ul>
            	
		<p class="copyright">&copy; 
              <script>var d = new Date();yr = d.getFullYear();document.write(yr);</script>
               Triton Trailers, LLC. All rights reserved.</p>
		

            </div>
    </div>
    
 </div><!-- END main content container -->  

<script type="text/javascript" src="/wp-content/themes/triton/js/retina-1.1.0.min.js"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4027977-3', 'auto');
  ga('send', 'pageview');

</script>
</body>
</html>