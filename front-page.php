<?php
/*
 * Front Page template
 *
 */

get_header(); ?>
	
    <div id="pageHeader" class="span_12 section">
    	<div class="span_12 homeslider">
        <div class="span_12 topPhotos">
            
        	<div <?php if (get_field('header_image_01', 163)){?> style="background: url(<?php the_field('header_image_01', 163); ?>); background-size: cover; background-position: center;"<?php }?> class="atv"></div>
            <div <?php if (get_field('header_image_2', 163)){?> style="background: url(<?php the_field('header_image_2', 163); ?>); background-size: cover; background-position: center;"<?php }?> class="snow"></div>
            <div <?php if (get_field('header_image_3', 163)){?> style="background: url(<?php the_field('header_image_3', 163); ?>); background-size: cover; background-position: center;"<?php }?> class="bike"></div>
        </div>
    
    	<div id="pageHeadline" class="span_12 group">
        	<div class="span_10 headline">
        		<h1>Built for adventure.</h1>
            </div>
        </div>
        
        <div class="span_12 bottomPhotos">
        	<div <?php if (get_field('header_image_4', 163)){?> style="background: url(<?php the_field('header_image_4', 163); ?>); background-size: cover; background-position: center;"<?php }?>  class="mower"></div>
            <div <?php if (get_field('header_image_5', 163)){?> style="background: url(<?php the_field('header_image_5', 163); ?>); background-size: cover; background-position: center;"<?php }?>  class="motorcross"></div>
            <div <?php if (get_field('header_image_06', 163)){?> style="background: url(<?php the_field('header_image_06', 163); ?>); background-size: cover; background-position: center;"<?php }?>  class="boat"></div>
        </div>
        </div>
    </div>

    <div class="section span_11 content">
	    <?php if(get_field('home_content', 163)): ?>
            <?php while(has_sub_field('home_content', 163)): ?>
                <div class="span_11 storyBlock group">
                    <div class="textBox">
                        <div class="span_6 text col">
                            <h5><?php the_sub_field('title', 163); ?></h5>
                            <p><?php the_sub_field('content', 163); ?></p>
                        </div>
                        <div class="span_6 imgBox col">
                            <img src="<?php the_sub_field('image_01', 163); ?>" alt="">
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
        
        <?php /*if(have_posts()){
		while (have_posts()){
			the_post(); 
			the_content();
			}
		}*/?>	
    </div>    
<?php get_footer(); ?>