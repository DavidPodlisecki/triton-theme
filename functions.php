<?php

	if (function_exists('add_theme_support')){
		add_theme_support('menus');
		}
	
	register_nav_menus(
		array(
    			'primary' => 'Main menu',
    			'secondary' => 'Secondary menu',
			'tertiary' => 'Footer menu',
			'mobile' => 'Mobile menu'
			)
		);
		
	// don't ping yourself
	function disable_self_ping( &$links ) {
    foreach ( $links as $l => $link )
        if ( 0 === strpos( $link, get_option( 'home' ) ) )
            unset($links[$l]);
		}
	add_action( 'pre_ping', 'disable_self_ping' );
	
	// ADD SEARCH FORM
	add_theme_support( 'html5', array( 'search-form' ) );
	
	// ADD FEATURED IMAGE
	add_theme_support( 'post-thumbnails' );


	// ADD VIDEO METABOX
	add_action( 'add_meta_boxes', 'featuredVideo_add_custom_box' );
	add_action( 'save_post', 'featuredVideo_save_postdata' );
	
	function featuredVideo_add_custom_box() {
    	add_meta_box(  'featuredVideo_sectionid', 'Featured Video', 'featuredVideo_inner_custom_box', 'post', 'side' );
		}
	
	function featuredVideo_inner_custom_box( $post ) {
    	wp_nonce_field( plugin_basename( __FILE__ ), 'featuredVideo_noncename' );
 
    	// show featured video if it exists
    	echo getFeaturedVideo( $post->ID, 260, 120);   
 
    	echo '<h4 style="margin: 10px 0 0 0;">URL [YouTube Only]</h4>';
    	echo '<input type="text" id="featuredVideoURL_field" name="featuredVideoURL_field" value="'.get_post_meta($post->ID, 'featuredVideoURL', true).'" style="width: 			100%;" />';
		}
		
	function featuredVideo_save_postdata( $post_id ) {
 
    // check autosave
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
      return;
 
    // check authorizations
    if ( !wp_verify_nonce( $_POST['featuredVideo_noncename'], plugin_basename( __FILE__ ) ) )
      return;
 
    // update meta/custom field
    update_post_meta( $post_id, 'featuredVideoURL', $_POST['featuredVideoURL_field'] );
}

function getFeaturedVideo($post_id) {
    $featuredVideoURL = get_post_meta($post_id, 'featuredVideoURL', true);
 
    preg_match('%(?:youtube\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $featuredVideoURL, $youTubeMatch);
 
    if ($youTubeMatch[1]){
		$short = do_shortcode('[fve]http://youtu.be/'.$youTubeMatch[1].'[/fve]');
		echo '<div class="span_12 group postImage">';
		echo $short;
		echo '</div>';
	}
    else{
        return null;
	}
}

	// Add class name to body tag

	/*function page_bodyclass() {  
		global $wp_query;
		$page = '';
		if (is_front_page() ) {
    	   		$page = 'home';
			} 
		elseif (is_page()) {
   	   		$page = $wp_query->query_vars["post_parent"];
			}
		if ($page)
			echo ' class="'. $page. '"';
}*/

function the_breadcrumb() {
	if (!is_home()) {
		echo '<a href="';
		echo get_option('home');
		echo '">';
		bloginfo('name');
		echo "</a>  ";
		if (is_category() || is_single()) {
			the_category('title_li=');
			if (is_single()) {
				echo "  ";
				the_title();
			}
		} elseif (is_page()) {
			echo the_title();
		}
	}
}


// TURN WP GALLERY INTO SLIDESHOW

add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';
	$captiontag = tag_escape($captiontag);
	$thumbs = array();


    $output = "<div class=\"slideshow-wrapper section span_11\">\n";
    $output .= "<div class=\"slide-nav\"><a class=\"prev\"></a><a class=\"next\"></a></div>\n";
	$output .= "<div class=\"slide-view section span_10\">\n";
    $output .= "<ul class=\"slider group span_12\">\n";
	

    foreach ($attachments as $id => $attachment) {
        
        $img = wp_get_attachment_image_src($id, 'full');
		$thumb = array_push($thumbs, wp_get_attachment_image_src($id, 'thumbnail'));
		
        $output .= "<li>\n";
		if ( $captiontag ) {
                if ( trim( $attachment->post_excerpt ) ) {
					$output .= "<span class=\"wp-caption-text gallery-caption\">\n";
                    $output .= wptexturize($attachment->post_excerpt). "</span>\n";
                }
		}
        $output .= "<img src=\"{$img[0]}\" width=\"{$img[1]}\" height=\"{$img[2]}\" alt=\"slideshow image\" />\n";
        $output .= "</li>\n";
    }

    $output .= "</ul>\n</div>\n";
	$output .= "<ul class=\"thumbs\">\n";
	foreach($thumbs as $id => $thumbnail){
		$output .= "<li class=\"thumb\"><img src=\"{$thumbnail[0]}\"></li>\n";
		}
	$output .= "</ul>\n";
    $output .= "</div>\n";
    return $output;
}


// FAQS POST TYPE

function create_faqs_post_type() {
  register_post_type( 'FAQs',
    array(
      'labels' => array(
        'name' => __( 'FAQs' ),
        'singular_name' => __( 'FAQ' ),
		'add_new' => __('Add New', 'FAQ'),
		'add_new_item' => __('Add New FAQ'),
		'edit_item' => __('Edit FAQ'),
		'new_item' => __('New FAQ'),
		'view_item' => __('View FAQ'),
		'search_items' => __('Search FAQs'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash')
      ),
	  	'taxonomies' => array('FAQ Category'),
      	'public' => true,
      	'has_archive' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '/wp-content/themes/triton/img/q.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 4,
		'supports' => array('title','editor','thumbnail')
    )
  );
}
add_action( 'init', 'create_faqs_post_type' );
register_taxonomy("FAQ_category", array("faqs"), array("hierarchical" => true, "label" > "FAQ Categories", "singular_label" => "FAQ Category", "rewrite" => true));

// OPTIONS POST TYPE

function create_opts_post_type() {
  register_post_type( 'Options',
    array(
      'labels' => array(
        'name' => __( 'Options' ),
        'singular_name' => __( 'Options' ),
		'add_new' => __('Add New', 'Options'),
		'add_new_item' => __('Add New Options'),
		'edit_item' => __('Edit Options'),
		'new_item' => __('New Options'),
		'view_item' => __('View Options'),
		'search_items' => __('Search Options'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash')
      ),
	  	'taxonomies' => array('Options Category'),
      	'public' => true,
      	'has_archive' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 4,
		'supports' => array('title','editor','thumbnail')
    )
  );
}
add_action( 'init', 'create_opts_post_type' );
register_taxonomy("Opts_category", array("options"), array("hierarchical" => true, "label" > "Option Categories", "singular_label" => "Option Category", "rewrite" => true));

// ACCESSORY POST TYPE

function create_acc_post_type() {
  register_post_type( 'accessory',
    array(
      'labels' => array(
        'name' => __( 'Accessories' ),
        'singular_name' => __( 'Accessory' ),
		'add_new' => __('Add New', 'Accessory'),
		'add_new_item' => __('Add New Accessory'),
		'edit_item' => __('Edit Accessory'),
		'new_item' => __('New Accessory'),
		'view_item' => __('View Accessory'),
		'search_items' => __('Search Accessories'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash')
      ),
	  	'taxonomies' => array('Accessory Category'),
      	'public' => true,
      	'has_archive' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => 'dashicons-lightbulb',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 4,
		'supports' => array('title','editor','thumbnail')
    )
  );
}
add_action( 'init', 'create_acc_post_type' );
register_taxonomy("Accessory_category", array("accessory"), array("hierarchical" => true, "label" > "Categories", "singular_label" => "Category", "rewrite" => true));

// CAREERS POST TYPE

function create_careers_post_type() {
  register_post_type( 'Careers',
    array(
      'labels' => array(
        'name' => __( 'Careers' ),
        'singular_name' => __( 'career' ),
		'add_new' => __('Add New', 'Career'),
		'add_new_item' => __('Add New Career'),
		'edit_item' => __('Edit Career'),
		'new_item' => __('New Career'),
		'view_item' => __('View Career'),
		'search_items' => __('Search Careers'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash')
      ),
      'public' => true,
      	'has_archive' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		'menu_icon' => '/wp-content/themes/triton/img/careerIcon.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => 5,
		'supports' => array('title','editor','thumbnail')
    )
  );
}
add_action( 'init', 'create_careers_post_type' );

if ( ! function_exists('product_support') ) {

// Register Custom Post Type
function product_support() {

	$labels = array(
		'name'                => _x( 'Product Support', 'Post Type General Name', 'text_domain' ),
		'singular_name'       => _x( 'Manual', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'           => __( 'Product Support', 'text_domain' ),
		'name_admin_bar'      => __( 'Post Type', 'text_domain' ),
		'parent_item_colon'   => __( 'Parent Item:', 'text_domain' ),
		'all_items'           => __( 'All Items', 'text_domain' ),
		'add_new_item'        => __( 'Add New Item', 'text_domain' ),
		'add_new'             => __( 'Add New', 'text_domain' ),
		'new_item'            => __( 'New Item', 'text_domain' ),
		'edit_item'           => __( 'Edit Item', 'text_domain' ),
		'update_item'         => __( 'Update Item', 'text_domain' ),
		'view_item'           => __( 'View Item', 'text_domain' ),
		'search_items'        => __( 'Search Item', 'text_domain' ),
		'not_found'           => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'text_domain' ),
	);
	$args = array(
		'label'               => __( 'product', 'text_domain' ),
		'description'         => __( 'Post Type Description', 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', ),
		'taxonomies'          => array( 'products'),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	register_post_type( 'product', $args );

}

// Hook into the 'init' action
add_action( 'init', 'product_support', 0 );

}

// Register Custom Taxonomy
function product_year_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Product Year', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Year', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Item', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_year_support', array( 'product' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'product_year_taxonomy', 0 );

// Register Custom Taxonomy
function product_type_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Product Type', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Year', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Type', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_type_support', array( 'product' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'product_type_taxonomy', 0 );

// Register Custom Taxonomy
function product_tag_taxonomy() {

	$labels = array(
		'name'                       => _x( 'Product Tag', 'Taxonomy General Name', 'text_domain' ),
		'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'text_domain' ),
		'menu_name'                  => __( 'Product Tag', 'text_domain' ),
		'all_items'                  => __( 'All Items', 'text_domain' ),
		'parent_item'                => __( 'Parent Item', 'text_domain' ),
		'parent_item_colon'          => __( 'Parent Item:', 'text_domain' ),
		'new_item_name'              => __( 'New Item Name', 'text_domain' ),
		'add_new_item'               => __( 'Add New Type', 'text_domain' ),
		'edit_item'                  => __( 'Edit Item', 'text_domain' ),
		'update_item'                => __( 'Update Item', 'text_domain' ),
		'view_item'                  => __( 'View Item', 'text_domain' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'text_domain' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'text_domain' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'text_domain' ),
		'popular_items'              => __( 'Popular Items', 'text_domain' ),
		'search_items'               => __( 'Search Items', 'text_domain' ),
		'not_found'                  => __( 'Not Found', 'text_domain' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'product_tag_support', array( 'product' ), $args );

}

// Hook into the 'init' action
add_action( 'init', 'product_tag_taxonomy', 0 );


add_image_size( 'overview_thumb', 350, 173, true );
/* Flush rewrite rules for custom post types. */
add_action( 'after_switch_theme', 'bt_flush_rewrite_rules' );

/* Flush your rewrite rules */
function bt_flush_rewrite_rules() {
     flush_rewrite_rules();
}

function get_terms_dropdown_year($taxonomies, $args){
            $myterms = get_terms($taxonomies, $args);
            $output ="<select name='product_year_support' class='span_6 colWrap'>"; 
            $output .="<option value=''>Model Year</option>"; 
            foreach($myterms as $term){
                    $root_url = get_bloginfo('url');
                    $term_taxonomy=$term->taxonomy;
                    $term_slug=$term->slug;
                    $term_name =$term->name;
                    $link = $term_slug;
                    $output .="<option value='".$link."'>".$term_name."</option>";
            }
            $output .="</select>";
    return $output;
    }
function get_terms_dropdown_type($taxonomies, $args){
            $myterms = get_terms($taxonomies, $args);
            $output ="<select name='product_type_support' class='span_6 colWrap'>"; 
            $output .="<option value=''>Choose Trailer Type</option>"; 
            foreach($myterms as $term){
                    $root_url = get_bloginfo('url');
                    $term_taxonomy=$term->taxonomy;
                    $term_slug=$term->slug;
                    $term_name =$term->name;
                    $link = $term_slug;
                    $output .="<option value='".$link."'>".$term_name."</option>";
            }
            $output .="</select>";
    return $output;
    }

 // Add Shortcode
function option_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
		), $atts )
	);

	// Code
return '<span class="opt"></span>';

}
add_shortcode( 'option', 'option_shortcode' );
 // Add Shortcode
function standard_shortcode( $atts ) {

	// Attributes
	extract( shortcode_atts(
		array(
		), $atts )
	);

	// Code
return '<span class="std"></span>';

}
add_shortcode( 'standard', 'standard_shortcode' );

// Add Shortcode
function small_shortcode( $atts , $content = null ) {

	// Code
return '<br><span style="font-size:.8em">'.$content.'</span>';
}
add_shortcode( 'small', 'small_shortcode' );

// Add Shortcode
function app_shortcode( $atts , $content = null ) {

	// Code
return '<br><a href="'.$content.'" class="apppdf" target="_blank"></a><br>';
}
add_shortcode( 'app', 'app_shortcode' );


function quote_button_shortcode( $atts ) {
	extract(shortcode_atts(array(
		'model' => 'model', 
		'cat' => '#'
	), $atts));
	
	$button = '<a href="/request-a-quote/?mz=' . esc_attr( $model ) . '&catz=' . esc_attr( $cat ) . '">Get A Quote</a>';
	return $button;
}
add_shortcode( 'quote', 'quote_button_shortcode' );

function block_search_filter( $query ) {
  if ( $query->is_search && $query->is_main_query() ) {
    $query->set( 'post__not_in', array( 117,577,299 ) ); 
  }
}
add_filter( 'pre_get_posts', 'block_search_filter' );



wpcf7_add_shortcode('cargooptions', 'createbox', true);
function createbox(){

	global $post;
	
	$model = $_GET['mz'];


$args = array(
	'post_type' => 'options',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'Opts_category',
			'field'    => 'slug',
			'terms'    => strtolower($model)
		
		),	
	),

);
	
	$myposts = get_posts( $args );

	
	foreach ( $myposts as $post ) : setup_postdata($post);

		
			$options = explode(',', get_the_content());
			
			// output list
			$output = '';
			foreach ($options as $key => $value){
						   
				$output .= '<input type="checkbox" name="option_'.$key.'" value="'.$value.'" id="opt'.$key.'" class="optionSelect">';
				$output .= '<label for="opt'.$key.'"  class="span_4 colWrap"><span></span> ' . trim($value) . '</label>';
			}
	
		endforeach;

	return $output;

}

function hiddenreferer_shortcode($tag) {

	if ( ! is_array( $tag ) )
		return '';

	$options = (array) $tag['options'];
	foreach ( $options as $option ) {
		if ( preg_match( '%^name:([-0-9a-zA-Z_]+)$%', $option, $matches ) ) {
			$name_att = $matches[1];
		}
	}

	 $pageURL = $_SERVER["HTTP_REFERER"];
	 $value_att = $pageURL;
	 $html = '<input type="hidden" name="' . $name_att . '" value="'.$value_att.'" />';
	 return $html;
}
wpcf7_add_shortcode('hiddenreferer', 'hiddenreferer_shortcode', true);



function filter_search($query) {
    if ($query->is_search) {
	$query->set('post_type', array('post', 'page', 'product','accessory'));
    };
    return $query;
};
add_filter('pre_get_posts', 'filter_search');


?>