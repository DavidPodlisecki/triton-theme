<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">


<link rel="apple-touch-icon" sizes="57x57" href="/wp-content/themes/triton/icons/apple-touch-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/wp-content/themes/triton/icons/apple-touch-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/wp-content/themes/triton/icons/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/wp-content/themes/triton/icons/apple-touch-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/wp-content/themes/triton/icons/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/wp-content/themes/triton/icons/apple-touch-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/wp-content/themes/triton/icons/apple-touch-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/wp-content/themes/triton/icons/apple-touch-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/wp-content/themes/triton/icons/apple-touch-icon-180x180.png">
<link rel="icon" type="image/png" href="/wp-content/themes/triton/icons/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="/wp-content/themes/triton/icons/android-chrome-192x192.png" sizes="192x192">
<link rel="icon" type="image/png" href="/wp-content/themes/triton/icons/favicon-96x96.png" sizes="96x96">
<link rel="icon" type="image/png" href="/wp-content/themes/triton/icons/favicon-16x16.png" sizes="16x16">
<link rel="manifest" href="/wp-content/themes/triton/icons/manifest.json">
<link rel="shortcut icon" href="/wp-content/themes/triton/icons/favicon.ico">
<meta name="msapplication-TileColor" content="#231f20">
<meta name="msapplication-TileImage" content="/wp-content/themes/triton/icons/mstile-144x144.png">
<meta name="msapplication-config" content="/wp-content/themes/triton/icons/browserconfig.xml">
<meta name="theme-color" content="#ffffff">


<?php 
	  wp_head();
?>

<title><?php wp_title( '|', true, 'left' ); ?></title>
<link rel="stylesheet" type="text/css" href="/wp-content/themes/triton/MyFontsWebfontsKit.css">
<link rel="stylesheet" type="text/css" href="/wp-content/themes/triton/style.css">
<link rel="stylesheet" type="text/css" href="/wp-content/themes/triton/js/jquery-ui-1.11.4.custom/jquery-ui.min.css">

<script type="text/javascript" src="/wp-content/themes/triton/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/triton/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript" src="/wp-content/themes/triton/js/triton.js"></script>

<script type="text/javascript" src="/wp-content/themes/triton/js/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
<script>
  $(function() {
    $( "#pdate" ).datepicker();
  });
  </script>


<!--[if IE]>
	<style>
    	#left-nav #searchMenu ul li form input{width:120px;margin-right:-10px}
    </style>
<![endif]-->
<!--[if lt IE 9]>
	<script src="/wp-content/themes/triton/js/html5shiv.js"></script>
	<script src="/wp-content/themes/triton/js/mediaqueries.js"></script>
<![endif]-->
<!--[if (gte IE 6)&(lte IE 8)]>
  <script type="text/javascript" src="/wp-content/themes/triton/js/selectivizr.js"></script>
<![endif]-->


</head>

<body>

<noscript><div id="j" class="span_12 section group">This site works best with Javascript enabled.</div></noscript>

<div id="bigRedMenu">
	<a class="closeBtn"></a>
	<?php wp_nav_menu(array('menu' => 'Secondary Menu')); ?>
</div>
<div id="bigRedSearch">
<div class="section group">
	<a class="closeSearchBtn"></a>
    </div>
		<form method="post" action="/find-a-dealer/" enctype="multipart/form-data">
            <label>Find a dealer
            <input type="text" name="zip" placeholder="Zip/Postal"></label>
            <input type="submit" value="submit" class="submit">
        </form>
        <?php get_search_form(); ?>
        <div id="socialMobile" class="section">
        	<div class="section group">
        		<a href="https://www.facebook.com/pages/Triton-Trailers/123721674316653" class="facebook col" target="_blank"></a>
                <a href="https://www.youtube.com/user/TritonTrailers" class="youtube col" target="_blank"></a>
            </div>
        </div>
</div>
<div id="wrapper" class="section span_12">
<div id="left-nav">
			
            <div class="span_12 section mobileHeader group">
            	<a class="span_2 col secLink"><img src="/wp-content/themes/triton/img/hamburger.png" alt="Secondary Menu Button"></a>
            	<a href="/" class="span_8 col">
                	<img src="/wp-content/themes/triton/img/logo.png" alt="Triton Trailers Logo" class="logo"> <!-- SITE LOGO -->
                </a>
                <a class="span_2 col srchLnk"><img src="/wp-content/themes/triton/img/search.png" alt="Search Button"></a>
            </div>
        	
        	<div class="section span_10 content">
            
            	<a href="/" class="span_12 section logoLink">
                	<img src="/wp-content/themes/triton/img/logo.png" alt="Triton Trailers Logo" class="logo"> <!-- SITE LOGO -->
                </a>
                <div class="section span_12 trailerMobile">
                	<a>Trailers</a>
                </div>
                <?php wp_nav_menu(array('menu' => 'Main Menu')); ?>
            </div>
            
            <!-- THE MENU BELOW THE MENU -->
            <div id="floatingBox" class="group">
              <ul>
                  <li class="col span_12 findBtn"><a href="/find-a-dealer/">find a dealer</a></li>
                  
                  <li class="left spec secondMenu"><a class="secBtn">menu</a></li>
                  <li class="right spec searchMenu"><a class="srchBtn">search</a></li>
              </ul>
        	</div>
        
        	<!-- THE HIDDEN SECONDARY MENU -->
            <div id="secondaryMenu">
            	<?php wp_nav_menu(array('menu' => 'Secondary Menu')); ?>
        	</div>
            
            <!-- THE HIDDEN SEARCH MENU -->
            <div id="searchMenu">
            	
				<ul>
                	<li>
                    	<form method="post" action="/find-a-dealer/" enctype="multipart/form-data">
                        <label>Find a dealer
                        	<input type="text" name="zip" placeholder="Zip/Postal"></label>
                            <input type="submit" value="submit" class="submit">
                        </form>
                    </li>
                    <li>
                    <?php get_search_form(); ?>
                    </li>
                </ul>
                
        	</div>
        </div>
	
    	
       
    
    	<div id="main" class="section span_12"> <!-- main content container -->
        