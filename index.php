<?php
/**
 * The template for displaying all pages
 *
 */


get_header(); 

?>

		<?php get_template_part('templates/pagehead', 'default'); ?>
        
    
		<div class="section span_11 content">
        
        	<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; ?>
        	
		</div>
	

<?php get_footer(); ?>