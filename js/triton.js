// JavaScript Document

	
	// SET UP GLOBALS
	var w = $(window).width(), h = $(window).height();	
	var curIndex = null;
	var slides = null;
	var viewer = null;
	var slider = null;
	var curSlide = null;
	var curThumb = null;
	var cols = null;
	var lastCols = null;
	var prevCol = null;
	var nextCol = null;
	var specH = null;
		
	// CORRECT WIDTH OF NAVIGATION ELEMENTS BASED ON DEVICE WIDTH	
	function checkEverything(){
		if($(window).width() >= 1500){
			$('#wrapper').css({margin: '0 auto', left:'', width:1500});
			$('#left-nav').css({position: 'absolute'});
			$('#left-nav .menu-main-menu-container').css('display', 'block');
			$('#left-nav .sub-menu, #secondaryMenu, #searchMenu').css({width:0, display:'none'});
			$('#left-nav .sub-menu').css({top:'', paddingTop : '', height : '',left:'210px'});
			$('#left-nav .menu-item-has-children,.secondMenu,.searchMenu').data('clicked', false);
			}
		if($(window).width() >= 640 && $(window).width() <= 1499){
			var winW = $(window).width();
			$('#wrapper').css({width: winW-210, left:0, margin:''});
			if(winW != w){ // BECAUSE MOBILE SCROLL TRIGGERS RESIZE
			  $('#left-nav').css({position: ''});
			  $('#left-nav .menu-main-menu-container').css('display', 'block');
			  $('#left-nav .sub-menu, #secondaryMenu, #searchMenu').css({width:0, display:'none'});
			  $('#left-nav .sub-menu').css({top:'', paddingTop : '', height : '',left:'210px'});
			  $('#left-nav .menu-item-has-children,.secondMenu,.searchMenu').data('clicked', false);
			  }
			w = $(window).width();
			}
	else if($(window).width() <= 639){
			var winW = $(window).width();
			$('#wrapper').css({width: '100%'});
			if(winW != w){ // BECAUSE MOBILE SCROLL TRIGGERS RESIZE
				$('#left-nav').css({position: 'fixed', left:0, margin:''});
				$('#left-nav .menu-main-menu-container').css('display', 'none');
				$('#left-nav .sub-menu').css({top : '0', paddingTop : '0', height : 'auto', left : 0});
				$('#left-nav .sub-menu, #left-nav #secondaryMenu,#left-nav #searchMenu').slideUp(0).css({width:'100%'});
				$('#left-nav .menu-item-has-children,.secondMenu,.searchMenu').data('clicked', false);
				}
			w = $(window).width();
			}
		}
		
		// FOR PAGES WITH SLIDESHOW - RESET TO FIRST SLIDE ON PAGE LOAD & WINDOW RESIZE
		function slideReset(){
			if($('.slideshow-wrapper')){	
				slides = $('.slideshow-wrapper .slider li');
				viewer = $('.slideshow-wrapper .slide-view');
				slider = $('.slideshow-wrapper .slider');
				var sliderW = slides.length * 100;
				var slideW = 100 / slides.length;
				slides.css('width', slideW + '%');
				slider.css('width', sliderW + '%');
				curSlide = $('.slider li:eq(0)');
				curThumb = $('.thumbs img:eq(0)');
				$('.thumbs img, .slider li').removeClass('cur');
				slider.css('margin-left', '');
				curSlide.addClass('cur');
				curThumb.addClass('cur');
				curIndex = 0;
			}
		}
		
		// GET NEXT SLIDE
		function nextSlide(num){
			curSlide = $('.slider .cur');
			curThumb = $('.thumbs .cur');
			if(num < slides.length - 1){
				var nextIndex = num + 1;
				var next = $('.slider li:eq(' + nextIndex + ')');
				var newThumb = $('.thumbs img:eq(' + nextIndex + ')');
				var marg = nextIndex * 100;
				slider.animate({marginLeft: -marg+'%'}, 600);
				curSlide.removeClass('cur');
				curThumb.removeClass('cur');
				next.addClass('cur');
				newThumb.addClass('cur');
				curIndex = nextIndex;
				}
			else{
				slider.animate({marginLeft: 0}, 600);
				curSlide.removeClass('cur');
				curThumb.removeClass('cur');
				$('.slider li:eq(0), .thumbs img:eq(0)').addClass('cur');
				curIndex = 0;
				}
			}
		
		// GET PREVIOUS SLIDE
		function prevSlide(num){
			curSlide = $('.slider .cur');
			curThumb = $('.thumbs .cur');
			if(num != 0){
				var prevIndex = num - 1;
				var marg = 100 * prevIndex;
				var prev = $('.slider li:eq(' + prevIndex + ')');
				var newThumb = $('.thumbs img:eq(' + prevIndex + ')');
				slider.animate({marginLeft: -marg+'%'}, 600);
				curSlide.removeClass('cur');
				curThumb.removeClass('cur');
				prev.addClass('cur');
				newThumb.addClass('cur');
				curIndex = prevIndex;
				}
			else{
				var total = slides.length -1;
				var prev = $('.slider li:eq(' + total + ')');
				var prevThumb = $('.thumbs img:eq(' + total + ')');
				var marg = 100 * total;
				slider.animate({marginLeft: -marg+'%'}, 600);
				curSlide.removeClass('cur');
				curThumb.removeClass('cur');
				prev.addClass('cur');
				prevThumb.addClass('cur');
				curIndex = total;
				}
			}
			
	// COUNT SPEC TABLE COLUMNS AND ADD NUMERIC CLASS NAMES
	function addClasses(){
		cols = $('.specs tr:first').children('th');
		lastCols = $('.specs tr').children();
		$('.specs tr').each(function(){
				for(i = 0; i < cols.length; i++){
					var col = $(this).children().eq(i);
					col.addClass('col'+i);
					}
				});
		}
	// SET UP SPEC TABLE SLIDER FOR DIFFERENT WINDOW SIZES		
	function specTable(){
		
		if($(window).width() >= 800){
				nextCol = 5; //SET UP MAX NUMBER OF COLUMNS TO SHOW INITIALLY
				prevCol = 1;
				specH = $('.table').height();
				var padH = specH -[specH / 2.2];
				$('.tableArrowR, .tableArrowL').css('height', padH);
				$('.tableArrowR,.tableArrowL').css({paddingTop: specH / 2.2});
				$('.tableArrowR, .tableArrowL').hide(0);
				lastCols.show();
				if(cols.length > 5){ 
					$('.tableArrowR').show(0);
					for(i = 5; i < cols.length; i++){ 
						$('.col'+i).hide(0);
						}
					}
				else{$('.tableArrowR').hide(0);}
				}
		else if($(window).width() >= 640){
				nextCol = 4; //SET UP MAX NUMBER OF COLUMNS TO SHOW INITIALLY
				prevCol = 1;
				specH = $('.table').height();
				var padH = specH -[specH / 2.2];
				$('.tableArrowR, .tableArrowL').css('height', padH);
				$('.tableArrowR,.tableArrowL').css({paddingTop: specH / 2.2});
				$('.tableArrowR, .tableArrowL').hide(0);
				lastCols.show();
				if(cols.length > 4){ 
					$('.tableArrowR').show(0);
					for(i = 4; i < cols.length; i++){ 
						$('.col'+i).hide(0);
						}
					}
				else{$('.tableArrowR').hide(0);}
				}
		else if($(window).width() <=639){
			nextCol = 3; //SET UP MAX NUMBER OF COLUMNS TO SHOW INITIALLY
			prevCol = 1;
			specH = $('.table').height();
			var padH = specH -[specH / 2.2];
			$('.tableArrowR, .tableArrowL').css('height', padH);
			$('.tableArrowR,.tableArrowL').css({paddingTop: specH / 2.2});
			$('.tableArrowR, .tableArrowL').hide(0);
			if(cols.length > 3){ 
				$('.tableArrowR').show(0);
				for(i = 3; i < cols.length; i++){ 
					$('.col'+i).hide(0);
					}
				}
			else{$('.tableArrowR').hide(0);}
			}
		}
		
		
	// CHECK SIZES ON MOBILE ORIENTATION CHANGES	
	$(window).bind('orientationchange', function(e) {
		checkEverything();
		});
		
	
	
$(document).ready(function(){
		
		
		checkEverything();
		slideReset();
		
		// BECAUSE CSS CHILD SELECTOR ISN'T ALWAYS WORKING PROPERLY
		$('#main .content p:eq(0)').addClass('illum');
		$('.storyBlock p').removeClass('illum');
		$('.post,.job').last().css('border-bottom', 'none');
		$('#main .span_9 p img').addClass('full-width');
		
	// GET THE SUBMENU FOR THE CURRENTLY SELECTED NAV ITEM
	$('#left-nav .menu-item-has-children').on('click', this, function(e){
		var clicked = $(this).data('clicked');
		var docH = $(document).height();
		var topPos = $(this).offset().top;
		var menuTop = $('#menu-main-menu').offset().top;
		var submenu = $(this).find('.sub-menu');
		$('#left-nav #secondaryMenu,#left-nav #searchMenu').css({display: 'none' , width:0});

		if($(window).width() >= 640){
			if(!clicked){
				$('#left-nav .sub-menu').css({display: 'none', width: 0});
				$('#left-nav .menu-item-has-children,#left-nav .secondMenu,#left-nav .searchMenu').data('clicked', false);
				submenu.css({display:'block', top:-[topPos+1], paddingTop : menuTop, height : docH-[menuTop]});
				submenu.animate({width:'168px'},450);
				$(this).data('clicked', true);
				}
			else{
				submenu.animate({width:0},250, 
				function(){
					submenu.css('display','none');
					submenu.css({top:'', paddingTop : '', height : ''});
					});
				$(this).data('clicked', false);
				}
			}
		else if($(window).width() <= 639){
			if(!clicked){
				$('#left-nav .sub-menu').slideUp(50);
				$('#left-nav .menu-item-has-children,#left-nav .secondMenu,#left-nav .searchMenu').data('clicked', false);
				submenu.slideDown(300);
				$(this).data('clicked', true);
				}
			else{
				submenu.slideUp(150);
				$(this).data('clicked', false);
				}
			}
			e.preventDefault(); // PREVENT NAVIGATION ON PRIMARY MENU CLICK
		});
			// ALLOW NAVIGATION FOR SUB MENU CLICK
			$('#left-nav .sub-menu a').on('click', this, function(){
				var loc = $(this).attr('href');
				window.location = loc;
				});
			
			// GET THE SECONDARY MENU FOR THE HAMBURGER NAV ITEM
			$('#left-nav .secondMenu').on('click', this, function(e){
			  var clicked = $(this).data('clicked');
			  if(!clicked){
				  $('#left-nav .sub-menu, #left-nav #searchMenu').css({display:'none',width:0});
				  $('#left-nav .menu-item-has-children,#left-nav .searchMenu').data('clicked', false);
				  $('#left-nav #secondaryMenu').css('display', 'block');
				  $('#left-nav #secondaryMenu').animate({width:'168px'},450);
				  $(this).data('clicked', true);
				  }
			  else{
				  $('#left-nav #secondaryMenu').animate({width:0},250, 
				  function(){
					  $('#left-nav #secondaryMenu').css('display','none');
					  });
				  $(this).data('clicked', false);
					}
			  });
			  
		 // GET THE SEARCH MENU FOR THE SEARCH NAV ITEM
			$('#left-nav .searchMenu').on('click', this, function(e){
			  var clicked = $(this).data('clicked');
			  if(!clicked){
				  $('#left-nav .sub-menu,#left-nav #secondaryMenu').css({display:'none',width:0});
				  $('#left-nav .menu-item-has-children,#left-nav .secondMenu').data('clicked', false);
				  $('#left-nav #searchMenu').css('display', 'block');
				  $('#left-nav #searchMenu').animate({width:'168px'},450);
				  $(this).data('clicked', true);
				  }
			  else{
				  $('#left-nav #searchMenu').animate({width:0},250, 
				  function(){
					  $('#left-nav #searchMenu').css('display','none');
					  });
				  $(this).data('clicked', false);
					}
			  });
		
			
		  // HIDE MENU ON CLICK OUTSIDE
		  function hideStuff(){
			  if($(window).width() >= 640){
				  $('#left-nav .sub-menu, #left-nav #secondaryMenu,#left-nav #searchMenu').animate({width:0},200,
					  function(){
						  $('#left-nav .sub-menu, #left-nav #secondaryMenu').css('display','none')
						  $('#left-nav .sub-menu').css({top:'', paddingTop : '', height : ''});
						  });
				  $('#left-nav .menu-item-has-children,.secondMenu,.searchMenu').data('clicked', false);
				  }
			  }
		  $('html').click(function() {
		  		hideStuff();
		  		});
		  
		  $('#left-nav').click(function(event){
			  	event.stopPropagation();
		  		});
		  
		var mobileScroll = '';	
		// MOBILE MENU SHOW & HIDE
		$('.trailerMobile a').on('click', this, function(e){
			var clicked = $(this).data('clicked');
			var menu = $('#left-nav .menu-main-menu-container');
			if(!clicked){
				mobileScroll = $(document).scrollTop();
				menu.slideDown(350); 
				$('html,body').delay(350).animate({scrollTop: 0},0,
					function(){
						$('#left-nav').css('position','absolute');
						});
				$(this).css('background-position', '93% -11px');
				$(this).data('clicked', true);
				}
			else{
				$('#left-nav').css({position:'fixed',top:0});
				menu.slideUp(150);
				$('html,body').animate({scrollTop: mobileScroll},0);
				$(this).css('background-position', '93% 30px');
				$(this).data('clicked', false);
				}
			
			});
			
			
			// GET THEM BIG RED BOXES
			// secondary menu box
			$('.closeBtn').on('click', this, function(){
				if(mobileScroll == 0){
					$('#bigRedMenu').animate({marginLeft: '-100%'}, 250, 
						function(){
							$('#bigRedMenu').css('display', 'none');
							});
					}
				else{
					$('html,body').animate({scrollTop: mobileScroll},0,
						function(){
							var getHeight = $('#wrapper').height();
							$('#bigRedMenu').css({height: getHeight, position: 'fixed'});
							});
					$('#bigRedMenu').animate({marginLeft: '-100%'}, 200, 
						function(){
							$('#bigRedMenu').css({display: 'none', height: '', position: 'absolute'});
							});
					}
				});
			$('.secLink').on('click', this, function(){
				mobileScroll = $(document).scrollTop();
				if(mobileScroll == 0){
					$('#bigRedMenu').css('display', 'block');
					$('#bigRedMenu').animate({marginLeft: 0}, 450);
					}
				else{
					$('#bigRedMenu').css({display: 'block', position: 'fixed'});
					$('#bigRedMenu').animate({marginLeft: 0}, 450,
						function(){
							$('html,body').animate({scrollTop: 0},0);
							$('#bigRedMenu').css({position: 'absolute'});
							});
					}
				});
			//search box	
			$('.closeSearchBtn').on('click', this, function(){
				$('#bigRedSearch').animate({marginLeft: '-100%'}, 250, 
					function(){
						$('#bigRedMenu').css('display', 'none');
						});
				});
			$('.srchLnk').on('click', this, function(){
				$('#bigRedSearch').css('display', 'block');
				$('#bigRedSearch').animate({marginLeft: 0}, 450);
				});
				
		// PRELOAD IMAGES
		$.preloadImages = function() {
			var dir = '/wp-content/themes/triton/img/';
			for (var i = 0; i < arguments.length; i++) {
			  $('<img>').attr('src', dir+arguments[i]);
			}
		  }

		$.preloadImages(
			'closeX.png',
			'closeX@2x.png',
			'hamburger.png',
			'hamburger@2x.png',
			'logo.png',
			'logo@2x.png',
			'search.png',
			'search@2x.png',
			'searchArrow.png',
			'searchArrow@2x.png'
			);	
		
		// FLOAT EVERY OTHER HOMEPAGE IMAGE RIGHT
		$('#main .storyBlock .text:odd').css('float', 'right');
		$('#main .storyBlock .imgBox:odd').css('text-align', 'left');
		
		// TRAILER PAGES SUBMENU WRAPPING
		var items = $('.sub .sub-menu li');
		if(items.length <=4){
			items.css({width: '25%'});
			}
		if(items.length >=6){
			items.css({height: '50.1%'});
			for(i=0; i < 5; i++){
				$('.sub .sub-menu li:eq('+i+')').css('border-bottom', '1px solid #fff');
				}
			}
		
		// TRAILER SLIDESHOW TRIGGERS
		$('.next').on('click', this, function(){
			var num = curIndex;
			nextSlide(num);
			});
			
		$('.prev').on('click', this, function(){
			var num = curIndex;
			prevSlide(num);
			});
			
		$('.thumbs .thumb').on('click', this, function(){
			var num = $('.thumbs .thumb').index(this);
			if(num > curIndex){
				num = num + 1;
				prevSlide(num);
				}
			if(num < curIndex){
				num = num - 1;
				nextSlide(num);
				}
			else{}
			});
		
		// TRAILER SLIDESHOW MOBILE SWIPE TRIGGERS
		$('.slideshow-wrapper .slider li img').swipe({
			swipeLeft:function(e) {
			  nextSlide(curIndex);
			},
			swipeRight:function(e){
			  prevSlide(curIndex);
			  }
			});
		
		
		// BLOG POST SHARE LINK SHOW AND HIDE
		if($('.getLink')){
			$('.linkview').hide();
			$('.getLink').on('click', this, function(){
				var clicked = $(this).data('clicked');
				if(!clicked){
					$('.linkview').show(100).focus();
					$(this).data('clicked', true);
					}
				else{
					$('.linkview').hide(100);
					$(this).data('clicked', false);
					}
				});
			}
			
			// SHOW BLOG POST COMMENT FORM
			$('#postForm .postComment').on('click', this, function(){
				var form = $('#postForm #respond');
				var h = form.height();
				$('.postComment').hide();
				$('#postForm').animate({height: h},200);
				});
				
			// SPEC TABLE SLIDER CLICK FUNCTIONS
			$('.tableArrowR').on('click', this, function(){
				var last = cols.length-1;
				var tablePos = $('.specs tr th:first').width();
				$('.tableArrowR, .tableArrowL').hide(0,function(){
					$('.specs tr th:first').css({minWidth:tablePos});
					$('.col'+prevCol).hide(200);
					});
				$('.col'+nextCol).show(200, function(){
					$('.specs tr th:first').css({minWidth:''});
					$('.tableArrowL').css({left: tablePos + 14});
					$('.tableArrowL').show(0);
					});
				if(nextCol != last){
						nextCol = nextCol+1;
						prevCol = prevCol+1;
						$('.tableArrowR').show(0);
						}
					else{
						$('.tableArrowR').hide(0);
						}
				});
			$('.tableArrowL').on('click', this, function(){
				var first = 1;
				var tablePos = $('.specs tr th:first').width();
				$(this).hide(0, function(){
					$('.specs tr th:first').css({minWidth:tablePos});
					$('.tableArrowL').css({left: tablePos + 14});
					});
				$('.col'+nextCol).hide(200);
				$('.col'+prevCol).show(200, function(){
					$('.specs tr th:first').css({minWidth:''});
					$('.tableArrowR').show(0);
					});
				if(prevCol != first){
						nextCol = nextCol-1;
						prevCol = prevCol-1;
						$(this).show(0);
						}
					else{
						$(this).hide(0);
						}
				});
				
				
				$('.homeslider').swipe({
						swipeLeft:function(){
							if($(window).width() <= 639){
								var slideW = $('.homeslider').width()/2;
								$(this).animate({marginLeft:-slideW},350, 'swing');
								}
							},
						swipeRight:function(){
							if($(window).width() <= 639){
								$(this).animate({marginLeft:0},350, 'swing');
								}
							}
						});
						  
  				$('.sub .sub-menu .current_page_item a').addClass('active');
			
		});
		
		
	$(window).resize(function(){
		if($(window).width() >= 1500){
			$('#wrapper').css({margin: '0 auto', left:'', width:1500});
			$('#left-nav').css({position: 'absolute'});
			$('.homeslider .topPhotos div, .homeslider .bottomPhotos div').css('width', '');
			$('.homeslider').css({marginLeft: 0})
			}
		if($(window).width() >= 640 && $(window).width() <= 1499){
			$('#bigRedMenu, #bigRedSearch').css({marginLeft: '-100%', display: 'none'});
			$('.homeslider .topPhotos div, .homeslider .bottomPhotos div').css('width', '');
			$('.homeslider').css({marginLeft: 0});
			}
		if($(window).width() != w){ // BECAUSE MOBILE DEVICES TRIGGER RESIZE ON SCROLL
			checkEverything();
			slideReset();
			specTable();
			}
		if($(window).width <= 639){
			widths = $('.homeslider').width()/6;
			$('.homeslider .topPhotos div, .homeslider .bottomPhotos div').css('width', widths);
			$('.homeslider').css({marginLeft: 0});
			}
		});	
	
	$(window).load(function(e) {
		// MAKE SURE CLASSES ARE ADDED TO COLUMNS BEFORE SETTING UP SPEC TABLE SLIDER
		$(window).queue(firstFunc).queue(secFunc);
		function firstFunc(next) {
			$('.tableArrowR, .tableArrowL').hide(0);
			addClasses();
			next();
			}
		function secFunc(next) {
			specTable();
			next();
			}
		
        checkEverything();
		slideReset();
    	});
