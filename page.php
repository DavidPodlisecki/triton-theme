<?php
/**
 * The template for displaying all pages
 *
 */


get_header(); 


?>

		<?php get_template_part('templates/pagehead', 'default'); ?>
        
    
		<div class="section span_11 content">
        
        	<div class="span_12 group">
            	<div class="span_9 col">
				  <?php while ( have_posts() ) : the_post(); ?>
                      <?php the_content(); ?>
                  <?php endwhile; ?>
				</div>
                <div class="span_3 col">
              	<?php get_sidebar(); ?>
              </div>
			</div>	
            
		</div>
	

<?php get_footer(); ?>