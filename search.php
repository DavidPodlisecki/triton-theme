<?php
/**
 * Template Name: Search Page
 *
 */


get_header(); 


global $query_string;

$query_args = explode("&", $query_string);
$search_query = array();

foreach($query_args as $key => $string) {
	$query_split = explode("=", $string);
	$search_query[$query_split[0]] = urldecode($query_split[1]);
} // foreach

$search = new WP_Query($search_query);


?>
		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>

		<div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						Search Results
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content searchResults">
        	<div class="span_12 group">
            <div class="span_9 col">
        	<?php
			global $wp_query;
			$total_results = $wp_query->found_posts;
			?>
            
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php if( is_page(array(82,169))) continue; ?>
            <div class="span_12 section post">
            <?php $checktheposttype = get_post_type( get_the_ID() );
            
            if ($checktheposttype == 'product') { ?>
            
              <?php
                        $pdfhref = '';
                        if (get_field('product_manual')=='') {$pdfhref = get_the_content();}
                        else  {$pdfhref = get_field('product_manual');}
                        ?>
                      <h5 class="post-title"><?php the_title();?>  <?php $terms_as_text = get_the_term_list( $post->ID, 'product_type_support', '', ', ', '' ) ; echo strip_tags($terms_as_text, '');?> <?php $terms_as_text = get_the_term_list( $post->ID, 'product_year_support', '', ', ', '' ) ; echo strip_tags($terms_as_text, '');?></h5>
               <a href="<?php echo $pdfhref; ?>" target="_blank">Download Product Support PDF</a>
            </div>
            <?php } else { ?>
              <h5 class="post-title">
              <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
              </h5>
              <?php the_excerpt(__('Continue reading »','example')); ?>
              <a class="moreLink" href="<?php the_permalink() ?>">read more</a>
            </div>
          <?php } 
          
     
          endwhile; else: ?>
          <div class="entry span_12 section"><p><?php _e('Sorry, no posts matched your Search criteria.'); ?></p></div>
          <?php endif; ?>
          
          <?php
            $big = 999999999; // need an unlikely integer

echo paginate_links( array(
	'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
	'format' => '?paged=%#%',
	'current' => max( 1, get_query_var('paged') ),
	'total' => $wp_query->max_num_pages
) );
     s        
          ?>
          
          </div>
          <div class="span_3 col">
              	<?php get_sidebar(); ?>
              </div>
            </div>
		</div>
	

<?php get_footer(); ?>