<?php
/**
 * The Sidebar containing the main widget area
 */
?>
<?php get_the_category_list(); ?>

<div class="span_10 section sideBar">

<ul class="cat">
<span class="sidebarTitle">Categories</span>
<?php

wp_list_categories('title_li=&exclude=1&hide_empty=0' . $GLOBALS[asides_id]);

?>
</ul>

<ul class="recent">
<span class="sidebarTitle">Recent Posts</span>
<?php
	$args = array( 'numberposts' => '5' );
	$recent_posts = wp_get_recent_posts( $args );
	foreach( $recent_posts as $recent ){
		echo '<li><a href="' . get_permalink($recent["ID"]) . '">' .   $recent["post_title"].'</a> </li> ';
	}
?>

</ul>


<ul class="arch">
<span class="sidebarTitle">Archives</span>
<?php wp_get_archives( array( 'type' => 'postbypost', 'limit' => 20, 'format' => 'custom' ) ); ?>
</ul>
</div>