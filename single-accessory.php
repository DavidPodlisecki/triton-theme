<?php

//Template Name: single accessory


get_header(); 
?>
		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>

	<div id="header" class="span_12 section accessories">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						ACCESSORIES
                    </h2>
                </div>
        	</div>
         </div>
         <div class="span_11 center">
         	<a href="javascript:history.back()" class="backLink"></a>
         </div>
        
    
		<div class="section span_11 content">
        
        	<h5><?php echo get_the_title( $post->ID ) ?></h5>
            
        	<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>
			
            <p class="bold">Contact dealer for pricing</p>
            
            <?php the_post_thumbnail(); ?>
			
		</div>
	

<?php get_footer(); ?>