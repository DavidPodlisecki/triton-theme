<?php
/**
 * The template for displaying all single posts
 *
 */


get_header(); 

?>
		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>

		<div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						News &amp; Media
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content single">
        
        <div class="span_12 section breadcrumbs"><?php the_breadcrumb(); ?></div>
        <div class="span_12 group">
             <div class="span_9 col newsPost">
                  
                  <?php if(the_post_thumbnail()){ ?>
					 <div class="span_12 section postImage">
                    	<?php  the_post_thumbnail( array(200,220) ); ?>
                    </div>
                  <?php } 
				  		else{$the_video_output = getFeaturedVideo($post->ID);
							?>
							<?php 
								 
								echo $the_video_output;
							?>
                  	<?php } ?>
                  
                  <h5>
                  <?php
                      echo single_post_title();
                  ?>
                  </h5>
                  
                  <span class="postDate-main"><?php the_time('F jS, Y'); ?></span>
                  <?php while ( have_posts() ) : the_post(); ?>
                      <?php the_content(); ?>
                  <?php endwhile; ?>
                  
                  <div class="span_12 section shareLinks">
                  	Share this: 
                    	<a target="_blank" href="http://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="fb-share"></a>
                        <a href="http://twitter.com/share?url=<?php the_permalink(); ?>" target="_blank" class="tw-share"></a>
                        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank" class="gp-share"></a>
                        <a class="getLink"></a><input type="text" class="linkview" value="<?php the_permalink(); ?>" autofocus>
                  </div>
 
				  	<?php 
						$zero = '';
						$one = '<h6>1 Comment</h6>';
						$more = '<h6>% Comments</h6>';
						comments_number( $zero, $one, $more );
					?>
                    
                    <?php  if(comments_open()){ ?>
                        <div id="postForm">
                            <a class="postComment"></a>
                            <?php get_template_part('templates/comment-form') ?>
                        </div>
					<?php } ?>
                  
                  	<?php comments_template(); ?>
                  
                  
              </div>
              
              <div class="span_3 col">
              	<?php get_sidebar(); ?>
              </div>
              
             </div>    
		</div>
	

<?php get_footer(); ?>