<?php
/*
Template Name: Form Page
*/



get_header(); 


?>

		<?php get_template_part('templates/pagehead', 'default'); ?>
		
<?
 if (isset($_GET['catz'])) {
 
 if ( $_GET['catz']=="cargo")
 {
 	?>
		<style>
			#eldealerselector
			{
				display: none;
			}
		</style> 	
 	<?
 }
 else
 { ?>
	 <style>
		#cargo_options
			{
				display: none;
			}
	</style>
 <? }
 
}

?>  


<?php 

if (is_page( 651 )) {?>
<script>
function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}



$( document ).ready(function() {
$('input[name=your-name]').val(readCookie('name'));
$('input[name=your-email]').val(readCookie('email'));
$('input[name=model_num]').val(readCookie('model'));
$('input[name=serial_num]').val(readCookie('serial'));
$('input[name=purch_date]').val(readCookie('pdate'));
$('input[name=phone]').val(readCookie('phone'));
});


</script>

<? } ?>

		<div class="section span_11 content formPage">
        
        	<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>

		</div>
	

<?php get_footer(); ?>