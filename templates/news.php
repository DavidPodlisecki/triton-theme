<?php
/*
Template Name: News & Media
*/



get_header(); 


?>		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>		

		<script type="text/javascript">
			$(document).ready(function(e) {
                $('.sideBar .recent').css('display', 'none');
            	});
        	
        </script>
        <div id="header" class="span_12 section">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						News / Media
                    </h2>
                </div>
        	</div>
         </div>
        
    
		<div class="section span_11 content newsPage">
        
        	<div class="span_12 group">
            	<div class="span_9 col">
				  
				  
<?php query_posts('post_type=post&post_status=publish&posts_per_page=8&paged='. get_query_var('paged')); ?>

	<?php if( have_posts() ): ?>

        <?php while( have_posts() ): the_post(); ?>

	    <div <?php post_class(); ?>>
        
			<?php if(the_post_thumbnail()){ ?>
        		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(200,220) ); ?></a>
				<?php } 
				  		else{$the_video_output = getFeaturedVideo($post->ID);
							?>
							<?php 
								 
								echo $the_video_output;
							?>
                  	<?php } ?>
                
                
                <h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>

                

		<?php the_excerpt(__('Continue reading »','example')); ?>
		<a class="moreLink" href="<?php the_permalink() ?>">read more</a>
        
			<span class="postDate-main"><?php the_time('F jS, Y'); ?></span>
            </div><!-- /#post-<?php get_the_ID(); ?> -->

        <?php endwhile; ?>

		<div class="postnavigation">
			<div class="newer"><?php previous_posts_link(__('« newer posts','example')) ?></div> <div class="older"><?php next_posts_link(__('older posts »','example')) ?></div>
		</div><!-- /.navigation -->

	<?php else: ?>

		<div id="post-404" class="noposts">

		    <p><?php _e('None found.','example'); ?></p>

	    </div><!-- /#post-404 -->

	<?php endif; wp_reset_query(); ?>
                  
				</div>
                <div class="span_3 col sideBar">
                	<?php get_sidebar(); ?>
                </div>
			</div>	
		</div>
	

<?php get_footer(); ?>