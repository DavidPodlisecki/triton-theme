		<?php
						
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$typeimage = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $typeimage ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>		

		<div id="header" class="span_12 section accessories">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
						<?php
							if(get_post_custom($post->ID)){
								  $custom = get_post_custom($post->ID);
								  $type = $custom ["Category"][0];
								  $type = trim($type);
								  echo $type;
								}
							else{
								echo empty( $post->post_parent ) ? get_the_title( $post->ID ) : get_the_title( $post->post_parent );
								}
						?>
                    </h2>
                </div>
        	</div>
         </div>
         <div class="span_11 center">
         	<a href="javascript:history.back()" class="backLink"></a>
         </div>