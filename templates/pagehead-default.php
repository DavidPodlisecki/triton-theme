<?php
						
							empty( $post->post_parent ) ? $pagetitle = get_the_title( $post->ID ) : $pagetitle = get_the_title( $post->post_parent );
							
							/*
							 * create a random page type selection for choosing a header image
							 */
							
							$types = array('snowmobile', 'atv_utv', 'enclosed', 'pwc', 'motorcycle', 'utility'); 
							$rand_type = array_rand($types, 1);
							
							$type = $types[$rand_type];
							
						?>	
		<style>
		
		#main #header{
			background: url(/wp-content/themes/triton/img/<?php echo $type ?>Header.jpg) center top no-repeat;
			-webkit-background-size: cover;
			-moz-background-size: cover;
			-o-background-size: cover;
			background-size: cover;
			}
		
		</style>			
					
		
		<div id="header" class="span_12 section trailers" style="margin-bottom:40px">
        
        	<div class="span_11 pageTitle">
            	<div class="vertAlign span_10">
            		<h2>
					
                    <?php
						echo $pagetitle;
						?>
                    
                    </h2>
                </div>
        	</div>
         </div>