<?php
/*
Template Name: Product Support
*/



get_header(); 


?>

        <?php get_template_part('templates/pagehead', 'default'); ?>
        
    
		<div class="section span_11 content">
        	
            <div class="span_12 section">
            
        		<?php while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; ?>
                
            </div>
            <div class="span_12 section">
            	<h6>Trailer Parts &amp; Diagrams</h6>

              
 
                <form id="supportselectform" class="span_12 group">
                        <div>
                            <?php
                            $taxonomies = array('product_type_support'); //CHANGE ME!
                            $args = array('orderby'=>'name','hide_empty'=>false);
                            $select = get_terms_dropdown_type($taxonomies, $args);
                            $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
                            echo $select;
                            ?>
                            <?php
                            $taxonomies = array('product_year_support'); //CHANGE ME!
                            $args = array('orderby'=>'name', 'order'=>'DESC', 'hide_empty'=>false);
                            $select = get_terms_dropdown_year($taxonomies, $args);

                            $select = preg_replace("#<select([^>]*)>#", "<select$1 onchange='return this.form.submit()'>", $select);
                            echo $select;
                            ?>
                            <input type="button" name="submit" value="search" class="submit span_6 colWrap" id="ssformsubmit" />
                        </div>
                    </form>

          <div class="narrowList span_12 group">
            <a class="span_2 col filter active" id="all" >All</a>
          <?php //list terms in a given taxonomy
                $taxonomy = 'product_tag_support';
                $tax_terms = get_terms($taxonomy);
          ?>
          <ul>
          <?php
          foreach ($tax_terms as $tax_term) {
          echo  '<a class="span_2 col filter" id="' .  $tax_term->slug . '" ' . '>' . $tax_term->name.'</a>';
          }
          ?>
          </ul>

          
         </div>
          <script>
             function loadTable () {
                var producttype = $( "select[name=product_type_support] option:selected").val();
                var theyear = $( "select[name=product_year_support] option:selected").val();
                var weID = "all";
                
                if (theyear == '')
                {
                	theyear = $( "select[name=product_year_support] option:eq( 1 )").val();
                
                }
                //alert(producttype);
                //alert (theyear);
                // CHANGE GET URL TO LIVE SITE //
                //http://triton.equitycreative.co/the-results/ LIVE RESULTS
                //http://localhost:8888/the-results/ TEST RESULTS
                $.get( "/the-results/", {tag: weID, t: producttype, y: theyear } )
                .done(function( data ) {
                //alert( "Data Loaded: " + data );
                $('#theresults').empty();
                $('#theresults').append(data);
                });
            }   
            $('#ssformsubmit').click(function() {
                  loadTable();
               
            });
          $('.filter') .click(function() {
              if($('.filter').hasClass('active')){
                 $('.filter').removeClass('active');
                 $(this).addClass('active')
              }else{
                $(this).addClass('active')
              }
              
              var weID = $(this).attr('id');
                var producttype = $( "select[name=product_type_support] option:selected").val();
                var theyear = $( "select[name=product_year_support] option:selected").val();
                //alert(weID);
                //alert (theyear);
                // CHANGE GET URL TO LIVE SITE //
                //http://triton.equitycreative.co/the-results/ LIVE RESULTS
                //http://localhost:8888/the-results/ TEST RESULTS
                $.get( "/the-results/", { tag: weID, t: producttype, y: theyear } )
                .done(function( data ) {
                //alert( "Data Loaded: " + data );
                $('#theresults').empty();
                $('#theresults').append(data);
                });
          });
		var curPage = 0;
		function getRows(page){
			$('.prev, .next').hide();
			var rows = $('#theresults .diagrams tr');
			var maximum = 21;
			var lastRow = rows.length;
			var pages = Math.ceil(lastRow/maximum);
			var start = page * maximum;
			var end = maximum + start;
			//console.log(pages);
			if(rows.length >= maximum+1){
				if(start == 0){
					$('.next').show();
					$('#theresults .diagrams tr').hide(0,function(){
					for(i=0; i < maximum; i++){
						$('#theresults .diagrams tr:eq('+i+')').show();
						}
						$('#theresults .diagrams tr:eq(0)').show();
						
						});
					
					}
				else if(start >= 1 && end < lastRow){
					$('.prev,.next').show();
					$('#theresults .diagrams tr').hide(0, function(){
					for(i=start; i < [start+maximum]; i++){
						$('#theresults .diagrams tr:eq('+i+')').show();
						}
						$('#theresults .diagrams tr:eq(0)').show();
						});
					}
				else{
					$('.next').hide();
					$('.prev').show();
					$('#theresults .diagrams tr').hide(0, function(){
					for(i=start; i < [start+maximum]; i++){
						$('#theresults .diagrams tr:eq('+i+')').show();
						}
						$('#theresults .diagrams tr:eq(0)').show();
						});
					}
				}
			else{
				$('#theresults .diagrams tr').show();
				$('.prev, .next').hide();
				}
				
			}
		function nextPage(){
			$('#theresults .diagrams tr').show();
			curPage++;
			getRows(curPage);
			var top = $('#theresults .diagrams tr:eq(0)').offset().top;
			$('html,body').animate({scrollTop:top},300);
			}
		function prevPage(){
			$('#theresults .diagrams tr').show();
			curPage--;
			getRows(curPage);
			var top = $('#theresults .diagrams tr:eq(0)').offset().top;
			$('html,body').animate({scrollTop:top},300);
			}
		function getCatName(){
			var newName = '';
			if($( "select[name=product_type_support] option:selected").val() != ''){
				var name = $( "select[name=product_type_support] option:selected").val();
				switch(name){
					case 'accessory':
						newName = 'Accessory';
						break;
					case 'snowmobile':
						newName = 'Snowmobile';
						break;
					case 'atv_utv':
						newName = 'ATV/UTV';
						break;
					case 'enclosed_cargo':
						newName = 'Enclosed Cargo';
						break;
					case 'watercraft_pontoon':
						newName = 'Watercraft/Pontoon';
						break;
					case 'utility':
						newName = 'Utility';
						break;
					case 'motorcycle':
						newName = 'Motorcycle';
						break;
					}
				$('.catNameCol').each(function(){
					var checkAccess = $(this).text();
					if(checkAccess.indexOf('Accessory') > -1){
						$(this).html('Accessory');
						}
					else{
						$(this).html(newName);
						}
					});
				}
			if($('#accessory').hasClass('active')){
				var newName = 'Accessory';
				$('.catNameCol').html(newName);
				}
			
			}
		$(document).ajaxComplete(function(){
				curPage=0;
				getRows(curPage);
				getCatName();
			});
		
          $(window).load(function(){
                  
                  loadTable();
                  $('.prev, .next').hide();
                });
            
                  </script>
			     <div id="theresults"></div>
                   
				<div class="span_6 section pageNav"><a class="prev" onclick="prevPage()"><< previous</a><a class="next" onclick="nextPage()">next >></a></div>
            
                                
          </div>
            	
		</div>
	

<?php get_footer(); ?>