<?php
/*
Template Name: product support results
*/

$cur_year = date("Y");

$product_type = $_GET["t"];
$product_year = $_GET["y"];
$product_tag = $_GET["tag"];

if (($product_type == '') && ($product_year != '') && ($product_tag == '')){
$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $product_year_none,
		
		),
		
	),

);
}



elseif (($product_type == '') && ($product_year != '') && ($product_tag == 'all')){
$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $product_year_none,
		
		),
		
	),

);
}

elseif (($product_type == '') && ($product_year == '')){
$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $cur_year,
		
		),
		array(
			'taxonomy' => 'product_tag_support',
			'field'    => 'slug',
			'terms'    => $product_tag,
		
		),
	),

);



}
elseif (isset($product_type, $product_year, $product_tag)){
	if ( $product_tag == "all"){
$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_type_support',
			'field'    => 'slug',
			'terms'    => $product_type,
		),
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $product_year,
		
		),
		
	),

);
	}else {
	$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_type_support',
			'field'    => 'slug',
			'terms'    => $product_type,
		),
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $product_year,
		
		),
		array(
			'taxonomy' => 'product_tag_support',
			'field'    => 'slug',
			'terms'    => $product_tag,
		
		),
	),

);

	}

}
else {

$args = array(
	'post_type' => 'product',
	'posts_per_page' => -1,
	'orderby' => 'title',
	'order' => 'ASC',
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'product_type_support',
			'field'    => 'slug',
			'terms'    => $product_type,
		),
		array(
			'taxonomy' => 'product_year_support',
			'field'    => 'slug',
			'terms'    => $product_year,
		
		),
		array(
			'taxonomy' => 'product_tag_support',
			'field'    => 'slug',
			'terms'    => $product_tag,
		
		),
	),
);

} 
       


// The Query
$wp_query = new WP_Query( $args );
// The Loop
if ( $wp_query->have_posts() ) { ?>
	<table width="100%" class="span_12 section diagrams">
                  <tbody class="span_12">
                    <tr>
                      <th scope="col" width="30%">Category</th>
                      <th scope="col" width="60%">Brand/Style</th>
                      <th scope="col" width"10%">Year</th>
                    </tr>
	<?php while ( $wp_query->have_posts() ) {
		$wp_query->the_post(); ?>
		<tr>
                      <td class="catNameCol"><?php $terms_as_text = get_the_term_list( $post->ID, 'product_type_support', '', ', ', '' ) ; echo strip_tags($terms_as_text, '');?></th>
                      
                      <?php
                        $pdfhref = '';
                        if (get_field('product_manual')=='') {$pdfhref = get_the_content();}
                        else  {$pdfhref = get_field('product_manual');}
                        ?>
                      <td><a href="<?php echo $pdfhref; ?>" target="_blank"><?php the_title();?></a></td>
                      <td><?php $terms_as_text = get_the_term_list( $post->ID, 'product_year_support', '', ', ', '' ) ; echo strip_tags($terms_as_text, '');?></td>
        </tr>
	<?php } ?>
	 </tbody>
    </table>
    
    
	<?php 
} else {
	echo '<h5 style="color:#dc0017; padding:20px 0; text-transform:none">No Trailer Parts or Diagrams Found for your search</h5>';
}
/* Restore original Post Data */
wp_reset_postdata();

               

                    
                 
