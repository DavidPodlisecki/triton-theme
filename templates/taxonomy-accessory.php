<?php




get_header(); 


?>

		<?php get_template_part('templates/pagehead', 'accessory'); ?>
        
    
		<div class="section span_11 content">
        
        	<h5><?php echo get_the_title( $post->ID ) ?></h5>
            
        	<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content(); ?>
			<?php endwhile; // end of the loop. ?>
			
            <p class="bold">Contact dealer for pricing</p>
            
            <?php the_post_thumbnail(); ?>
			
		</div>
	

<?php get_footer(); ?>