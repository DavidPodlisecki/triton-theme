<?php
/*
Template Name: Trailer Series
*/
	  


get_header(); 


?>
		
		<?php get_template_part('templates/pagehead', 'series'); ?>
        
        <div class="section span_11 content">
        
			<?php while ( have_posts() ) : the_post(); ?>
				<h5> <?php the_title() ?> </h5>
				<?php the_content(); ?>
			<?php endwhile; ?>
            
            
            <div class="featuresList section span_12">
            <h6>Features</h6>
            <?php if( get_field('brochure_pdf') ):?>
            <a target="_blank" class="brochpdf" href="<?php the_field('brochure_pdf'); ?>" ></a>
            <?php endif; ?>
            <!-- <a target="_blank" class="brochpdf"></a> -->
            <ul class="span_12 group">
                <?php while(has_sub_field('features_list')): ?>
                    <li><?php the_sub_field('features'); ?></li>
                <?php endwhile; ?>
            </div>
            

            <div class="specTable section span_12">
                <h6>Specifications</h6>
                <!-- <a target="_blank" class="specpdf"></a> -->
                <?php if( get_field('spec_pdf') ):?>
                <a target="_blank" class="specpdf" href="<?php the_field('spec_pdf'); ?>" ></a>
                <?php endif; ?>
                <div class="legend"><span class="standard">Standard</span><span class="optional">N/A</span></div>
                <div class="table span_12 section">
                    <div class="tableArrowL"></div>
                    <div class="tableArrowR"></div>
                       <?php  $table = get_field( 'spec_table' );
                        if ( $table ) {
                            echo '<table width="100%" class="span_12 specs">';
                        if ( $table['header'] ) {
                        echo '<thead>';
                            echo '<tr>';
                                foreach ( $table['header'] as $th ) {
                                    echo '<th>';
                                        echo $th['c'];
                                    echo '</th>';
                                }
                            echo '</tr>';
                        echo '</thead>';
                    }
                    echo '<tbody>';
                        foreach ( $table['body'] as $tr ) {
                            echo '<tr>';
                                foreach ( $tr as $td ) {
                                    echo '<td>';
                                        echo do_shortcode( $td['c'] );
                                    echo '</td>';
                                }
                            echo '</tr>';
                        }
                    echo '</tbody>';
                echo '</table>';
            }
             ?>
            </div>

            <?php if ( !( is_page('premium-plus') || is_page('premium') || is_page('value') ) ) {
                echo "<p>Consult with your nearest dealer about available options</p>";
            } else {
                echo "<p>Talk to your nearest dealer about full trailer customization options.</p>";
            } ?>


            </div>
            
            <div class="accessoriesSection section span_12">
            	<h6>Accessories</h6>
                <!-- <a target="_blank" class="accesspdf"></a> -->
                <?php if( get_field('accessories_pdf') ):?>
                <a target="_blank" class="accesspdf" href="<?php the_field('accessories_pdf'); ?>" ></a>
                <?php endif; ?>
                <ul class="span_12 group">
               <?php while(has_sub_fields('accessories')):
                    $acc = get_sub_field('accessory_page');
					$term_list = wp_get_post_terms( $acc->ID, 'Accessory_category', array("fields" => "all"));
					$accessories_array[$term_list[0]->name][] = '<a href="' . get_permalink($acc->ID) . '" >' . get_the_title($acc->ID) . '</a>';
					wp_reset_postdata();
                    
                    
                endwhile;?>
                </ul>
            <? 
            
            foreach($accessories_array as $key => $value)
{
    if (!is_array($value))
    {
        echo $value ."</br>" ;
    }
    else
    {
       echo '<h4>' . $key . '</h4><ul class="span_12 group">';

       foreach ($value as $key2 => $value2)
       {
           echo '<li class="span_3 colWrap">' . $value2 .'</li>';
       }

       echo "</ul>";
    }
}
?>
            

                
            </div>
        	

		</div>
	

<?php get_footer(); ?>