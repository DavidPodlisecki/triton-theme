<?php
/*
Template Name: Trailer Subcategory
*/

get_header(); 


?>
		
		<?php get_template_part('templates/pagehead', 'trailers'); ?>
        
        <div class="section span_11 content">
        
			<?php while ( have_posts() ) : the_post(); ?>
                <h5> <?php the_title() ?> </h5>
				<?php the_content(); ?>
			<?php endwhile; ?>
            
            <!-- <a href="#" class="compareSpecs"></a> -->
            <?php if( get_field('compare_specs') ):?>
                <a target="_blank" class="compareSpecs" href="<?php the_field('compare_specs'); ?>" ></a>
                <?php endif; ?>
        	<?php $child_pages = $wpdb->get_results("SELECT *    FROM $wpdb->posts WHERE post_parent = ".$post->ID."    AND post_type = 'page' ORDER BY menu_order", 'OBJECT');  ?>
            <?php if ( $child_pages ) : foreach ( $child_pages as $pageChild ) : setup_postdata( $pageChild ); ?>
                <div class="child-thumb span_12 section trailerBox group">
                    <div class="span_6 col">
                        <a href="<?php echo  get_permalink($pageChild->ID); ?>" ><?php echo get_the_post_thumbnail($pageChild->ID, 'full'); ?></a>
                    </div>

                    <div class="span_6 col trailerDescript">
                        <a href="<?php echo  get_permalink($pageChild->ID); ?>" ><h5><?php echo $pageChild->post_title; ?></h5></a>
                       
                        <?php if(get_field('trailer_description', $pageChild->ID)): ?>
                        
                        <ul>
                            <?php while(has_sub_field('trailer_description', $pageChild->ID)): ?>
                                <li><?php the_sub_field('description', $pageChild->ID); ?> </li>
                            <?php endwhile; ?>
                        </ul>
                        
                        <?php endif; ?>   
                        
                        <a href="<?php echo  get_permalink($pageChild->ID); ?>" title="<?php echo $pageChild->post_title; ?>" class="moreBtn"></a>
                    </div>
                </div>
            <?php endforeach; endif;?>
        
        	

		</div>
	

<?php get_footer(); ?>